package com.bitbucket.thinbus.srp6.demo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the user data stored on the server. Note that we wont be returning
 * xml there will be a jackson adaptor to return json.
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "user")
public class UserDetail implements Serializable {
	private static final long serialVersionUID = 3834913679440358199L;

	@XmlAttribute(name = "email")
	private String email;

	@XmlAttribute(name = "salt")
	private String salt;

	@XmlAttribute(name = "verifier")
	private String verifier;

	protected UserDetail() {
	}

	public UserDetail(String id, String salt, String verifier) {
		this.email = id;

		if (this.email == null || this.email.equals(""))
			throw new IllegalArgumentException("id must be defined: " + this.email);

		this.salt = salt;

		if (this.salt == null || this.salt.equals(""))
			throw new IllegalArgumentException("salt must be defined: " + this.salt);

		this.verifier = verifier;

		if (this.verifier == null || this.verifier.equals(""))
			throw new IllegalArgumentException("verifier must be defined: " + this.verifier);
	}

	public String getEmail() {
		return email;
	}

	public String getSalt() {
		return salt;
	}

	public String getVerifier() {
		return verifier;
	}

}

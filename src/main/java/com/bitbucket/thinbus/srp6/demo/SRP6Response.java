package com.bitbucket.thinbus.srp6.demo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the server challenge response. Note that we wont be returning xml
 * there will be a jackson adaptor to return json.
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "user")
public class SRP6Response implements Serializable {
	private static final long serialVersionUID = 831926480614053350L;

	@XmlAttribute(name = "B")
	private final String B;

	@XmlAttribute(name = "salt")
	private final String salt;

	@XmlAttribute(name = "M2")
	private final String M2;

	public SRP6Response(String salt, String B, String M2) {
		this.salt = salt;
		this.B = B;
		this.M2 = M2;
	}

	public SRP6Response(String m2) {
		this("null", "null", m2);
	}

	public SRP6Response(String salt, String b) {
		this(salt, b, "null");
	}

	public String getM2() {
		return M2;
	}

	public String getSalt() {
		return salt;
	}

	public String getB() {
		return B;
	}


}

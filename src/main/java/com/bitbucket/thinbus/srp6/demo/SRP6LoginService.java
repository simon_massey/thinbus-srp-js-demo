package com.bitbucket.thinbus.srp6.demo;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSessionSHA256;

/**
 * A javax.ws.rs RESTful Service which returns JSON and which implements the
 * backed to the page ported from https://github.com/RuslanZavacky/srp-6a-demo
 */
@Path("/")
public class SRP6LoginService {

	/**
	 * Used to lookup N and g set in web.xml
	 */
	@Context
	ServletContext context;

	/**
	 * This is a javax.ws.rs service shared by any active users. In order to do
	 * the challenge response we need to store the SRP6JavascriptServerSession
	 * for each user. You could use the HTTPSession. Here we will just hold
	 * things in memory using the email as the key
	 */
	final static ConcurrentHashMap<String, SRP6JavascriptServerSession> fakeLoginSessions = new ConcurrentHashMap<>();

	@POST
	@Path("/srp-6a-demo/login")
	@Produces("application/json")
	public Response login(@FormParam("email") String email, @FormParam("A") String A, @FormParam("M1") String M1) throws Exception {

		String N = context.getInitParameter("N");
		String g = context.getInitParameter("g");

		UserDetail ud = SRP6RegisterService.fakeUserRepository.get(email);
		if (ud == null) {
			System.err.println("user is not registered: "+email);
			return Response.status(Status.FORBIDDEN).build();
		}

		if (notValid(A, M1)) {
			System.err.println("A and M1 must both be left undefined or must both be defined");
			return Response.status(Status.FORBIDDEN).build();
		}

		SRP6JavascriptServerSession srpSession = null;
		
		if (notDefined(A)) {
			// browser is attempting initial login possibly after a page refresh
			// we
			// create a fresh server session
			srpSession = new SRP6JavascriptServerSessionSHA256(N, g);
			fakeLoginSessions.put(email, srpSession);
			
			try {
				String B = srpSession.step1(ud.getEmail(), ud.getSalt(), ud.getVerifier());
				System.out.println(String.format("session state created then moved to %s", srpSession.getState()));
				return Response.status(200).entity(new SRP6Response(ud.getSalt(), B)).build();
			} catch (Exception e) {
				System.err.println(e);
				return Response.status(Status.FORBIDDEN).build();
			}
		} else {
			// browser is responding to a challenge and is sending the proof of
			// password
			srpSession = fakeLoginSessions.get(email);

			if(srpSession.getState().equals("STEP_1")) {
				try {
					String M2 = srpSession.step2(A, M1);
					System.out
							.println(String
									.format("session state moved to %s with shard session key %s",
											srpSession.getState(),
											srpSession.getSessionKey(true)));
					return Response.status(200).entity(new SRP6Response(M2)).build();
				} catch (Exception e) {
					System.err.println(e);
					return Response.status(Status.FORBIDDEN).build();
				}
			} else {
				System.err.println("user is trying to pass A and M1 but we are not in state STEP_1 reload the browser page and start login from beginning");
				return Response.status(Status.FORBIDDEN).build();
			}
		}
	}

	/**
	 * 
	 * @param A
	 *            The browser ephemeral key
	 * @return Whether A is not null and not blank.
	 */
	private boolean notDefined(String A) {
		if (A == null || A.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Both must be defined to be valid
	 * 
	 * @param A
	 *            The browser ephemeral key
	 * @param M1
	 *            The browser proof of the shared session key.
	 * @return
	 */
	private boolean notValid(String A, String M1) {
		if (A == null) {
			if (M1 == null) {
				return false;
			} else {
				return true;
			}
		} else if (M1 == null) {
			return true;
		}

		if (A == "") {
			if (M1 == "") {
				return false;
			} else {
				return true;
			}
		} else if (M1 == "") {
			return true;
		}

		return false;
	}
}
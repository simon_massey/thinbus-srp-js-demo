# Thinbus SRP WebApp Demo

Copyright (c) Simon Massey, 2014-2015

**WARNING** This demo is no longer kept up to date as it is a little unrealistic. There is another demo which is a [Spring Security and Spring MVC SRP demo](https://bitbucket.org/simon_massey/thinbus-srp-spring-demo/overview) which is more like a realistic application and which is kept up to date. 
 
Demo of Secure Remote Password (SRP-6a) protocol implementation of a browser authenticating to a Java server using [Thinbus SRP6a Javascript](https://bitbucket.org/simon_massey/thinbus-srp-js) and [Nimbus SRP6a Java](https://bitbucket.org/connect2id/nimbus-srp) libraries. 

This is a Java port of [Ruslan Zazvacky's SRP PHP demo](https://github.com/RuslanZavacky/srp-6a-demo). It is very artificial as both the registration and login forms are shown on a single page. In a real application the registration form would only ever be shown to the user once. Logging in using the demo page doesn't take you into a main application. It only uses AJAX to confirm that login is successful. With a real application the login page upon successful login should GET the main application landing page. That would unload the login page which would delete the Thinbus SRP session object as recommended on the [Thinbus page](https://bitbucket.org/simon_massey/thinbus-srp-js). 

The demo only depends upon a JAX RS provider (RESTeasy) and uses JSON RESTful services. 

## Build Prerequisites

  - Java Platform (JDK 7+) http://www.oracle.com/technetwork/java/javase/downloads/index.html
  - Maven2 http://maven.apache.org/

## Building And Running

```sh
git clone https://bitbucket.org/simon_massey/thinbus-srp-js-demo
cd thinbus-srp-js-demo
mvn clean package tomcat7:run
```

The run command starts tomcat running at http://localhost:8080/

## License

```
   Copyright 2014-2015 Simon Massey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
   
End.